This program reads my CPU and GPU temperature, and changes the color of
the computer's LED lighting to a range between blue and red accordingly.

This program was made for my ASUS Prime B560M-A motherboard, with an
nVidia GTX 1060 video card, running Debian 11 Linux. The motherboard
uses an Asus Aura USB device, hence the name of this project.


The command to compile it is in a comment in auramonitor.cpp

Simply run the program at startup. You may need to run it as root, or set
user permissions on the appropriate /dev/hidraw device. I use the following
udev rule:

KERNEL=="hidraw*", ATTRS{busnum}=="1", ATTRS{idVendor}=="0b05", ATTRS{idProduct}=="19af", MODE="0666"


At this time, I have no intentions to provide support for any other
configuration. I am providing this code only as a jumping-off point for
others to create their own similar projects.


This project also serves as my first public git project, and I will be
using it to learn how to use this system.


For more of my work, visit http://blog.shanock.com
