#include <hidapi/hidapi.h>
#include <cstring> // memcpy, memset
//#include <fstream>

// modprobe usbmon
// cat /sys/kernel/debug/usb/usbmon/0u | grep ":005:"


enum
{
//    AURA_MODE_OFF                       = 0,        // OFF mode                             
    AURA_MODE_STATIC                    = 1,        // Static color mode                    
//    AURA_MODE_BREATHING                 = 2,        // Breathing effect mode                
//    AURA_MODE_FLASHING                  = 3,        // Flashing effect mode                 
//    AURA_MODE_SPECTRUM_CYCLE            = 4,        // Spectrum Cycle mode                  
//    AURA_MODE_RAINBOW                   = 5,        // Rainbow effect mode                  
//    AURA_MODE_SPECTRUM_CYCLE_BREATHING  = 6,        // Rainbow Breathing effect mode        
//    AURA_MODE_CHASE_FADE                = 7,        // Chase with Fade effect mode          
//    AURA_MODE_SPECTRUM_CYCLE_CHASE_FADE = 8,        // Chase with Fade, Rainbow effect mode 
//    AURA_MODE_CHASE                     = 9,        // Chase effect mode                    
//    AURA_MODE_SPECTRUM_CYCLE_CHASE      = 10,       // Chase with Rainbow effect mode       
//    AURA_MODE_SPECTRUM_CYCLE_WAVE       = 11,       // Wave effect mode                     
//    AURA_MODE_CHASE_RAINBOW_PULSE       = 12,       // Chase with  Rainbow Pulse effect mode
//    AURA_MODE_RANDOM_FLICKER            = 13,       // Random flicker effect mode           
//    AURA_MODE_MUSIC                     = 14,       // Music effect mode                    
//    AURA_MODE_DIRECT                    = 0xFF,     // Direct control mode                  
};



class RgbController {
    private:
        hid_device *handle;
        unsigned char   payload[65];
        unsigned char   currMode[2];

        // These values might be auto-detected and set, if this code is to be adapted to support more controllers
        ushort vendor_id = 0x0b05;
        ushort product_id = 0x19af;
        int minUpdateRate = 200; // This Asus controller can only handle changes down to 200ms (determined via experimentation)

        void clearPayload() {
            memset(payload, 0x00, sizeof(payload));
            payload[0x00]   = 0xEC;
        }
        void setMode(unsigned char mode) {
            if (currMode[0x01] != mode) {
                clearPayload();
                payload[0x01]   = 0x35; // Set Mode
                payload[0x05]   = mode;
                // Modify range to address more controllers
                for (int i = 0; i <= 1; i++) {
                    payload[0x02]   = i; // On Controller
                    hid_write(handle, payload, 65);
                }
                //clearPayload();
            }
        }

    public: 
        RgbController() {
            handle = hid_open(vendor_id, product_id, nullptr);
            /* if (!handle) {
                std::cerr << "Failed to open device" << std::endl;
            } */

            // Delete these later, to support more modes
            setMode(AURA_MODE_STATIC); 
            payload[0x01]   = 0x36; // Send Color
            payload[0x03]   = 4; // Not sure what this is, but only values of 4-7 work on my computer.
        }
        ~RgbController() {
            hid_close(handle);
        }
        int getMinUpdateRate() { return minUpdateRate; }
        void staticColor(unsigned char newColor[3]) {
            // Uncomment these later, to support more modes
            //setMode(AURA_MODE_STATIC); 
            //payload[0x01]   = 0x36; // Send Color
            //payload[0x03]   = 4; // Not sure what this is, but only values of 4-7 work on my computer.

            for (int i = 5; i < 13; i+=3)
                memcpy(&payload[i], newColor, 3);

            hid_write(handle, payload, 65);

        }
        /*	This is code originally intended to control individual LEDs, but I did not need it for this project. I may re-implement it later.
        void changeColor(unsigned char* led_data, unsigned char led_count) {
            for (int i = 0; i < leds; i+=20) {
                payload[0x04]   = (i + 20 >= leds)? leds - i : i + 20;
            }
            // Prepare payload for direct assignment
            payload[0x01]   = 0x40; //AURA_CONTROL_MODE_DIRECT;
            payload[0x02]   = 0x80;
            payload[0x03]   = start_led; // start_led
            payload[0x04]   = led_count; // led_count
            memcpy(&payload[0x05], led_data, led_count * 3);
            hid_write(handle, payload, 65);
        } */
};
