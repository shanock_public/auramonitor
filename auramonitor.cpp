#include <unistd.h> // sleep

#include "GpuHandler.cpp"
#include "LedManager.cpp"

// g++ -Wall auramonitor.cpp -o auramonitor -Wno-unused-variable -Wno-unused-but-set-variable -lhidapi-hidraw -lnvidia-ml

const int UPDATE_FREQUENCY = 1; // seconds

const int THRESH_CPU_MIN = 30;
const int THRESH_CPU_MAX = 80;
const int THRESH_CPU_RANGE = THRESH_CPU_MAX - THRESH_CPU_MIN;

const int THRESH_GPU_MIN = 30;
const int THRESH_GPU_MAX = 90;
const int THRESH_GPU_RANGE = THRESH_GPU_MAX - THRESH_GPU_MIN;


int getCpuTemp() {
    int currVal;
    std::ifstream newCpuTemp("/sys/class/thermal/thermal_zone1/temp");
    newCpuTemp >> currVal;
    newCpuTemp.close();
    return currVal/1000;
}

int main() {

    GpuHandler* gpuHandler = new GpuHandler();
    LedManager* ledManager = new LedManager();

    int cpuTemp, gpuTemp;
    int cpuVal, gpuVal, currVal = 0, oldVal = 0;

    while (true) {
        // Gather info
        cpuTemp = getCpuTemp();
        gpuTemp = gpuHandler->getTemp();

        // Decide if the ledManager needs to get involved
        cpuVal = (cpuTemp - THRESH_CPU_MIN) * 1024 / THRESH_CPU_RANGE;
        gpuVal = (gpuTemp - THRESH_GPU_MIN) * 1024 / THRESH_GPU_RANGE;
        currVal = std::max(cpuVal, gpuVal);
        if (currVal != oldVal) {
            oldVal = currVal;
            ledManager->shiftColor(currVal);
        } else {
            // Wait for next cycle
            sleep(UPDATE_FREQUENCY);
        }

    }
}

