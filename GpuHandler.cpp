#include <nvml.h>



class GpuHandler {
    private:
        nvmlDevice_t device;
        unsigned int tempInt;
    public:
    GpuHandler() {
        nvmlInit();
        nvmlDeviceGetHandleByIndex(0, &device);
    }
    ~GpuHandler() {
        nvmlShutdown();
    }
    int getTemp() {
        nvmlDeviceGetTemperature(device, NVML_TEMPERATURE_GPU, &tempInt);
        return tempInt;
    }
    /*
    int getClock() {
        nvmlDeviceGetClockInfo(device, NVML_CLOCK_SM, &tempInt);
        return tempInt;
    }
    int getMaxClock() {
        nvmlDeviceGetMaxClockInfo(device, NVML_CLOCK_GRAPHICS, &tempInt);
        return tempInt / 1000;
    }
    int getMemClock() {
        nvmlDeviceGetClockInfo(device, NVML_CLOCK_MEM, &tempInt);
        return tempInt * 2;
    }
    int getMaxMemClock() {
        nvmlDeviceGetMaxClockInfo(device, NVML_CLOCK_MEM, &tempInt);
        return tempInt * 2;
    }
    int getLoad() {
        nvmlUtilization_t temp;
        nvmlDeviceGetUtilizationRates(device, &temp);
        return temp.gpu;
    }
    int getMemLoad() {
        nvmlMemory_t temp;
        nvmlDeviceGetMemoryInfo(device, &temp);
        return temp.used / 1048576;
    }
    int getMemTot() {
        nvmlMemory_t temp;
        nvmlDeviceGetMemoryInfo(device, &temp);
        return
        temp.total / 1048576;
    }
    */
};