#include <fstream>
#include <unistd.h> // sleep
//#include <cstring> // memcpy, memset

#include "AsusAuraUSBController.cpp"

const int COLOR_BRIGHTNESS = 64;
const float COLOR_SATURATION = 0.75;

int colorSpin(int val, int native) {
    return std::min(std::max((int)(std::max(std::min((512 - std::abs(val - native)),255),0) * COLOR_SATURATION + COLOR_BRIGHTNESS),0),255);
}
// Converts a numeric value from 0 to 1023 into a color from blue (to green to yellow) to red
void valToHue(unsigned char* convrgb, int val) {
    convrgb[2] = colorSpin(val, 0);
    convrgb[1] = colorSpin(val, 512);
    convrgb[0] = colorSpin(val, 1024);
}


class LedManager {

    private:
        RgbController* rgbController;

        unsigned char oldRgb[3];
        unsigned char newRgb[3];
        int tnsRgb[3];
        unsigned char sndRgb[3];

        // Timing variables
        int timeFrame = 1;
        int intervalFrames;
        int intervalFrame; // Controller can only handle about 200ms min

    public:
        LedManager() {            
            rgbController = new RgbController();
            intervalFrame = rgbController->getMinUpdateRate();
            intervalFrames = timeFrame * 1000 / intervalFrame;
        }
        ~LedManager() {}

        // This sets the rate at which one color will face into another
        void changeRate(int seconds, int frames = 5) {
            int newRate = seconds * 1000 / frames;
            if (newRate >= rgbController->getMinUpdateRate()) { // Check against the controller's capabilities
                timeFrame = seconds;
                intervalFrames = frames;
                intervalFrame = newRate;
            }
        }

        // This takes a value between 0 and 1023
        void shiftColor(int val) {
            if (val < 0) val = 0;
            if (val > 1024) val = 1024;
            valToHue(newRgb, val);
            shiftColor();
        }
        // This takes a direct color in the form of a 3-character array
        void shiftColor(unsigned char* sentRgb) {
            memcpy(newRgb, sentRgb, 3);
            shiftColor();
        }
        void shiftColor() {
            // Get each R G B difference between old and new color
            for (int i = 0; i < 3; i++)
                tnsRgb[i] = newRgb[i] - oldRgb[i];
            // Calculate each step, and send them per the defined interval
            for (int i = 1; i <= intervalFrames; i++) {
                for (int j = 0; j < 3; j++)
                    sndRgb[j] = oldRgb[j] + tnsRgb[j] * ((float)i/intervalFrames);
                rgbController->staticColor(sndRgb);
                usleep(intervalFrame * 1000); // takes microseconds
            }
            // The new color is now the old color
            memcpy(oldRgb, newRgb, 3);
        }

};